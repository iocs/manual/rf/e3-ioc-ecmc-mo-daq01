# -----------------------------------------------------------------------------
# EtherCAT IOC for MO Data Acquisition
# -----------------------------------------------------------------------------
require essioc
require ecmccfg 7.0.1
require momuda

epicsEnvSet(P, "MO:")
epicsEnvSet(R, "RFS-DAQ-001:")
epicsEnvSet("IOC" ,"$(P)Ctrl-ECAT-001")
epicsEnvSet("ECMCCFG_INIT" ,"")
epicsEnvSet("SCRIPTEXEC" ,"$(SCRIPTEXEC="iocshLoad")")
epicsEnvSet("ECMC_VER" ,"7.0.1")
epicsEnvSet("NAMING", "ESSnaming")

# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

# run module startup.cmd (only needed at ESS  PSI auto call at require)
$(ECMCCFG_INIT)$(SCRIPTEXEC) ${ecmccfg_DIR}startup.cmd, "IOC=$(IOC),ECMC_VER=$(ECMC_VER="7.0.1"),EC_RATE=500"

# ecmc
iocshLoad("$(E3_CMD_TOP)/iocsh/modaq.iocsh")

# modaq
dbLoadRecords("$(momuda_DIR)/db/momuda.template","P=$(P), R=$(R), CH=CH1, PV=$(IOC):DS00-DataAct")
dbLoadRecords("$(momuda_DIR)/db/momuda.template","P=$(P), R=$(R), CH=CH2, PV=$(IOC):DS01-DataAct")
dbLoadRecords("$(momuda_DIR)/db/momuda.template","P=$(P), R=$(R), CH=CH3, PV=$(IOC):DS02-DataAct")
dbLoadRecords("$(momuda_DIR)/db/momuda.template","P=$(P), R=$(R), CH=CH4, PV=$(IOC):DS03-DataAct")

################
## Configure diagnostics:

ecmcConfigOrDie "Cfg.EcSetDiagnostics(1)"
ecmcConfigOrDie "Cfg.EcEnablePrintouts(0)"
ecmcConfigOrDie "Cfg.EcSetDomainFailedCyclesLimit(100)"

ecmcGrepParam *ds0*

################
## Go active:
$(SCRIPTEXEC) ($(ecmccfg_DIR)setAppMode.cmd)
iocInit()
