# e3-ioc-ecmc-mo-daq01

e3 ioc - EtherCAT Control for Data Acquisition of Master Oscillator at MBL070


## Running the IOC

This IOC doesn't need to be compiled nor installed in a specific directory. Just make sure that the versions of `ecmccfg`, `ecmc` and `momuda`are in `st.cmd.
If your E3 environment is activated, just go to the top directory of this IOC and run `iocsh.bash st.cmd`.
